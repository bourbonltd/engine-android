package sh.bourbon.engine_example

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import sh.bourbon.engine.BourbonEngine
import sh.bourbon.engine.BourbonEngineListener
import sh.bourbon.engine.EngineConfiguration
import sh.bourbon.engine.EngineRoute

class MainActivity : AppCompatActivity(), BourbonEngineListener {

    companion object {
        private const val BOURBON_ENGINE_NAME = "bourbonEngineExample"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mainRouteProperties = mutableMapOf<String, Any?>()
        mainRouteProperties["title"] = "Top Artists"
        mainRouteProperties["list"] = ArtistsMock.data()

        val bourbonEngine = BourbonEngine(this, BOURBON_ENGINE_NAME)
        bourbonEngine.setListener(this)
        bourbonEngine.setup(
            EngineConfiguration(
                organizationId = BuildConfig.ORGANIZATION_ID,
                projectId = BuildConfig.PROJECT_ID,
                engineEndpoint = BuildConfig.ENGINE_ENDPOINT,
                authenticationEndpoint = BuildConfig.AUTHENTICATION_ENDPOINT,
                engineVersion = BuildConfig.ENGINE_VERSION,
                configurationVersion = BuildConfig.CONFIGURATION_VERSION,
                engineRoute = EngineRoute("artists", mainRouteProperties.toMap())
            )
        )

        bourbonEngineView.setLifecycle(lifecycle)
        bourbonEngineView.setup(BOURBON_ENGINE_NAME)
    }

    override fun onBootstrapped() {
        Log.d(this::class.java.simpleName, "Bourbon Engine bootstrapped")
    }

    override fun onRouteLoaded(route: String) {
        Log.d(this::class.java.simpleName, "Bourbon Engine route loaded: $route")
    }

    override fun onRouteError(route: String) {
        Log.d(this::class.java.simpleName, "Bourbon Engine route error: $route")
    }

    override fun onError() {
        Log.d(this::class.java.simpleName, "Bourbon Engine error")
    }

    override fun onRouteChanged(newRoute: String) {
        Log.d(this::class.java.simpleName, "Bourbon Engine route changed: $newRoute")
    }

    override fun onTap(action: String, system: Boolean) {
        Log.d(this::class.java.simpleName, "Bourbon Engine tapped: $action, system: $system")
    }
}

package sh.bourbon.engine_example

import android.app.Application
import sh.bourbon.engine.BourbonEngine

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}
package sh.bourbon.engine

data class EngineConfiguration(
    val organizationId: String,
    val projectId: String,
    val engineEndpoint: String,
    val authenticationEndpoint: String,
    val engineRoute: EngineRoute? = null,
    val engineVersion: Double,
    val configurationVersion: Double
)
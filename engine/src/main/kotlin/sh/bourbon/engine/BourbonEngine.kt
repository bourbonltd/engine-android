package sh.bourbon.engine

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.FlutterEngineCache
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.plugin.common.MethodChannel
import java.util.*

class BourbonEngine(context: Context, private val name: String) {

    companion object BourbonEngine {
        private const val CHANNEL = "bourbon.sh/engine"
    }

    private var listener: BourbonEngineListener? = null
    private val engineCache by lazy { FlutterEngineCache.getInstance() }

    private val gson by lazy { Gson() }

    private val methodChannel by lazy {
        MethodChannel(requireFlutterEngine().dartExecutor, CHANNEL).apply {
            setMethodCallHandler { call, _ ->
                when (call.method) {
                    "bootstrapped" -> listener?.onBootstrapped()
                    "routeLoaded" -> {
                        getRouteFromArguments(call.arguments)?.let { listener?.onRouteLoaded(it) }
                    }
                    "routeError" -> {
                        getRouteFromArguments(call.arguments)?.let { listener?.onRouteError(it) }
                    }
                    "routeChanged" -> {
                        getRouteFromArguments(call.arguments)?.let { listener?.onRouteChanged(it) }
                    }
                    "error" -> listener?.onError()
                    "tap" -> {
                        val argumentsMap: Map<String, Any>? = call.arguments as? Map<String, Any>
                        val action = argumentsMap?.get("action") as? String
                        val system = argumentsMap?.get("system") as? Boolean
                        if (action != null && system != null) {
                            listener?.onTap(action, system)
                        }
                    }
                    else -> Log.d(this::class.java.simpleName, "Unhandled method: ${call.method}")
                }
            }
        }
    }


    init {
        val engine = FlutterEngine(context)
        engine.dartExecutor.executeDartEntrypoint(DartExecutor.DartEntrypoint.createDefault())

        engineCache.put(name, engine)
    }

    fun destroy() {
        engineCache.get(name)?.destroy()
        engineCache.remove(name)
    }

    private fun getRouteFromArguments(arguments: Any?): String? {
        val argumentsMap: Map<String, String>? = arguments as? Map<String, String>
        return argumentsMap?.get("route")
    }

    fun setListener(listener: BourbonEngineListener) {
        this.listener = listener
    }

    fun setup(configuration: EngineConfiguration) {
        var config = Configuration(
            configuration.organizationId,
            configuration.projectId,
            configuration.engineEndpoint,
            configuration.authenticationEndpoint,
            configuration.engineRoute?.route,
            configuration.engineRoute?.properties,
            configuration.engineVersion,
            configuration.configurationVersion
        )
        val jsonConfig = gson.toJson(config)
        methodChannel.invokeMethod("setup", jsonConfig)
    }

    fun updateRoute(route: EngineRoute, behaviour: RouteBehaviour) {
        val routeData = RouteData(route.route, route.properties, behaviour.name.toLowerCase(Locale.ROOT))
        val jsonRouteData = gson.toJson(routeData)
        methodChannel.invokeMethod("updateRoute", jsonRouteData)
    }

    private fun requireFlutterEngine(): FlutterEngine {
        return engineCache.get(name)
            ?: throw IllegalStateException("BourbonEngine must be initialized using BourbonEngine.init(context)")
    }
}

private data class Configuration(
    val organizationId: String,
    val projectId: String,
    val engineEndpoint: String,
    val authenticationEndpoint: String,
    val mainRoute: String? = null,
    val mainRouteProperties: Map<String, Any?>? = null,
    val engineVersion: Double,
    val configurationVersion: Double
)

private data class RouteData(
    val route: String,
    val properties: Map<String, Any?>? = null,
    val behaviour: String
)

interface BourbonEngineListener {
    fun onBootstrapped()

    fun onRouteLoaded(route: String)

    fun onRouteError(route: String)

    fun onError()

    fun onRouteChanged(newRoute: String)

    fun onTap(action: String, system: Boolean)
}
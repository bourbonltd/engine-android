package sh.bourbon.engine

data class EngineRoute(
    val route: String,
    val properties: Map<String, Any?>? = null
)
package sh.bourbon.engine

enum class RouteBehaviour { PUSH, RETAIN, SYSTEM }
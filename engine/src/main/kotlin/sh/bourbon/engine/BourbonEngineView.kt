package sh.bourbon.engine

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.flutter.embedding.android.FlutterFragment
import io.flutter.embedding.android.RenderMode
import io.flutter.embedding.android.TransparencyMode

class BourbonEngineView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs), LifecycleObserver {

    private var flutterFragment: FlutterFragment? = null

    init {
        View.inflate(context, R.layout.view_bourbon_engine, this)
    }

    fun setup(id: String, cleanCacheOnDestroy: Boolean = false) {
        initAndInflateFlutterFragment(id, cleanCacheOnDestroy)
    }

    fun setLifecycle(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        flutterFragment?.onStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        flutterFragment?.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        flutterFragment?.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        flutterFragment?.onStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        flutterFragment?.onDestroy()
        flutterFragment = null
    }

    private fun initAndInflateFlutterFragment(engineId: String, cleanCacheOnDestroy: Boolean) {
        flutterFragment = FlutterFragment.withCachedEngine(engineId)
            .renderMode(RenderMode.texture) // Required to animate the view
            .transparencyMode(TransparencyMode.transparent)
            .destroyEngineWithFragment(cleanCacheOnDestroy)
            .build()

        // AppCompatActivity is required for FlutterFragment inflation
        if (context !is AppCompatActivity) throw IllegalStateException("BourbonEngineView must be used in an AppCompatActivity")

        (context as AppCompatActivity).supportFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, flutterFragment as Fragment)
            .commit()
    }
}
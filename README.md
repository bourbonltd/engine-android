# Bourbon Engine

## Adding to your build.gradle

```gradle
implementation 'sh.bourbon:engine:1.+'

repositories {
    maven {
        url 'https://gitlab.com/api/v4/groups/bourbonltd/-/packages/maven'
    }
    maven {
        url 'https://storage.googleapis.com/download.flutter.io'
    }
}
```

Note: Make sure to use an x86_64 emulator. x86 is not supported.